import os, sys
sys.path.append(os.getcwd())

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.backends.cudnn as cudnn

import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader
from tqdm import tqdm

from config import Config # pylint: disable=no-name-in-module
from test import test_model
import utils

from os.path import join as pathjoin
import shutil, argparse, datetime, json
from IPython import embed
import pynvml

from algos.layerwisefeature import IntermediateFeatureExtractor

# == Arguments ==
parser = argparse.ArgumentParser()
parser.add_argument('-run', type=str, help='The indicator of run (default=1)', default='1')

g = parser.add_mutually_exclusive_group(required=False)
g.add_argument('-l', type=str,help='continue training from a specific model')
g.add_argument('-c', action='store_true', help='continue training from the latest model by the default scheme')
g.add_argument('-o', action='store_true', help='Overwrite everything and start training')

parser.add_argument('-lep', type=int, help='the epoch to start from')
parser.add_argument('-v', action='store_false', help='validate along the training process on the test set?')
parser.add_argument('-tfrefresh', action='store_true',help='Remove existing tensorboard records')

args = parser.parse_args()
validation_arg = args.v

conf = Config(run_number=args.run)

assert (((args.l is not None) and args.c) or ((args.l is not None) and args.o) or (args.c and args.o)) is not True, 'You may only choose one among continue, overwrite, and load.'

print('Performing experiments at {}'.format(os.getcwd()))
print('Run #{}'.format(args.run))
print('Log will be saved at {}'.format(conf.experiment_path))

start_epoch = -1
initial_net_dict = None

if args.l is not None:
    start_epoch = args.lep - 1
    assert args.lep is not None, 'You must specify a start epoch in argument -lep (if training from scratch, type -lep=0)'
    assert os.path.isfile(args.l), 'Invalid model'
    print('Loading {}, starting at epoch {}').format(args.l, args.lep)
    initial_net_dict = (torch.load(args.l))

elif os.path.isdir(conf.experiment_path):
    print('Previous Training Record Exists.')
    # Continue training by loading latest.pthl from the models directory
    if args.c:
        latest_model = pathjoin(conf.model_path, 'latest.pthl')
        assert os.path.isfile(latest_model), 'no valid latest.pthl'
        meta = torch.load(latest_model)
        assert isinstance(meta, dict)
        start_epoch = meta['epoch']
        print('Loading {}/latest.pthl, starting at epoch {}'.format(conf.model_path, start_epoch))
        initial_net_dict = meta['state_dict']
    
    # Overwrite everything
    elif args.o:
        print('Overwriting everything.')
        shutil.rmtree(conf.experiment_path)
        for direc in [conf.experiment_path, conf.model_path, conf.log_path]:
            os.mkdir(direc)
    
    else:
        print('Please either choose overwrite(-o), load(-l & -leq), or continue(-c) in argument')
        exit()

if args.tfrefresh:
    if os.path.isdir(conf.log_path):
        shutil.rmtree(conf.log_path)

for dirc in [conf.experiment_path, conf.model_path, conf.log_path, conf.vis_dir]:
    if not os.path.isdir(dirc):
        print(dirc)
        os.makedirs(dirc)

# == Initializing Network and Datasets ==
print('Initializing Network')
net_raw, device, handle = utils.prepare_net(conf.net(), conf.use_gpu)
if initial_net_dict is not None:
    net_raw.load_state_dict(initial_net_dict)


train_set = conf.dataset(train=True, transform=conf.train_transform)
train_loader = DataLoader(train_set, batch_size=conf.training_batchsize, shuffle=True, num_workers=conf.train_provider_count)

if validation_arg:
    val_set = conf.dataset(train=False, transform=conf.test_transform)
    val_loader = DataLoader(val_set, batch_size=conf.training_batchsize, shuffle=True, num_workers=conf.train_provider_count)

ife = IntermediateFeatureExtractor(net_raw, conf.layer_seq)
net = ife.net
# Tensorboard Log Writer
writer = SummaryWriter(conf.log_path)

# Visualizing Structure of the Network
dataiter = iter(train_loader)
images, labels = dataiter.next()
print(images.size(), labels.size())
try:
    writer.add_graph(net, images.to(device))
except:
    print('failed visualizing network structure in tensorboard.')
    pass

criterion = conf.criterion
optimizer = conf.optimizer_conf(net)
scheduler = conf.lr_scheduler_conf(optimizer)
if start_epoch == -1:
    torch.save(net.state_dict(), pathjoin(conf.model_path, 'epoch0.pth'))
utils.get_layer_info(net)

# Start Training
print('Start Training: {}'.format(datetime.datetime.now()))
for epoch in range(conf.epoch_num):  # loop over the dataset multiple times

    if epoch <= start_epoch:
        scheduler.step()
        continue
    running_loss = 0.0
    corrected, total = 0, 0
    start_time = datetime.datetime.now()
    p_norms, r_norms, grad_norms = {l:[] for l in conf.comp_layers}, {l:[] for l in conf.comp_layers}, {l:[] for l in conf.comp_layers}

    for i, data in enumerate(train_loader, 0):

        st = datetime.datetime.now()

        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        optimizer.zero_grad()

        ife_info, outputs = ife(inputs)
        loss = criterion(outputs, labels)
        loss.backward()

        if conf.proj_percentage != 0:

            assert conf.proj_percentage > 0 and conf.proj_percentage <= 1
            
            vect_grads = {var_name: var_param.grad.view(-1) for (var_name, var_param) in ife.net.named_parameters()}
            vect_grads_mat = {var_name: var_param.grad for (var_name, var_param) in ife.net.named_parameters()}

            for layer in conf.comp_layers:

                weight_key = layer + ".weight"
                assert weight_key in vect_grads
                vect_grad = vect_grads[weight_key]
                layer_x, layer_y = ife_info[layer]
                E_x = torch.Tensor.mean(layer_x, dim=0, keepdim=True)
                Id_y = torch.diag(torch.ones(layer_y.shape[1])) #pylint:disable=no-member
                E_x_basis = utils.kp_2d(E_x, Id_y)
                print(E_x_basis.shape)
                exit()

                # TODO:
                """
                I have completed the naive calculation of the basis which should project off (E_x_basis)
                What is left is do the projection
                """

                p_vect_grad = H_egvecs.transpose(0, 1).mv(H_egvecs.mv(vect_grad))
                remain_vect_grad = vect_grad - p_vect_grad

                grad_norm = torch.norm(vect_grad)
                remain_norm = torch.norm(remain_vect_grad)
                proj_norm = torch.norm(p_vect_grad)
                
                r_norms[layer].append((remain_norm / grad_norm).cpu().numpy())
                p_norms[layer].append((proj_norm / grad_norm).cpu().numpy())
                grad_norms[layer].append(grad_norm.cpu().numpy())
                
                if conf.off_eigenspace:
                    off_grad = vect_grad - p_vect_grad * conf.proj_percentage
                    grads_modified[weight_key] = off_grad
                else:
                    on_grad = p_vect_grad * conf.proj_percentage + (1 - conf.proj_percentage) * vect_grad
                    grads_modified[weight_key] = on_grad

                """
                Modify the code above
                """
            
            for var_name, var_param in net.named_parameters():
                if var_name in grads_modified:
                    assert grads_modified[var_name].view(-1).size() == var_param.grad.view(-1).size()
                    # print(var_name)
                    var_param.grad = grads_modified[var_name].view_as(var_param.grad).clone()

        # print(p_norms, r_norms, grad_norms)
        # exit()
        # HM.utils.timer(start_time, "{}".format(i))

        optimizer.step()

        # scheduler.step()
        step_count = epoch * len(train_loader) * conf.training_batchsize + i * conf.training_batchsize
        running_loss += loss.item()
        _, predicted = torch.Tensor.max(outputs.data, 1)
        corrected += (predicted == labels).sum().item()
        total += labels.size(0)
        update_freq = 100

        # Logging Training Loss
        if i % update_freq == update_freq - 1:
            
            avg_loss = running_loss / update_freq
            writer.add_scalar('train_loss', avg_loss, step_count)
            avg_acc = corrected / total
            writer.add_scalar('train_acc', avg_acc, step_count)

            writer.add_scalar('lr', scheduler.get_last_lr()[0], step_count)
            running_loss = 0.0
            corrected, total = 0, 0

            for layer in conf.comp_layers:
                writer.add_scalar('p_norm_{}'.format(layer), np.mean(p_norms[layer]), step_count)
                writer.add_scalar('r_norm_{}'.format(layer), np.mean(r_norms[layer]), step_count)
                writer.add_scalar('grad_norm_{}'.format(layer), np.mean(grad_norms[layer]), step_count)
            p_norms, r_norms, grad_norms = {l:[] for l in conf.comp_layers}, {l:[] for l in conf.comp_layers}, {l:[] for l in conf.comp_layers}
        
        # Logging GPU Performance
        if device != 'cpu':
            gpu_update_freq = 250
            if i % gpu_update_freq == update_freq - 1:
                mem_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
                load_info = pynvml.nvmlDeviceGetUtilizationRates(handle)
                writer.add_scalar('GPURAM_usage', mem_info.used / mem_info.total, step_count)
                writer.add_scalar('GPU_usage', load_info.gpu, step_count)
    
    # Creating Snapshots
    current_snapshot_path = pathjoin(conf.model_path, 'epoch{}.pth'.format(epoch + 1))
    if (epoch % conf.snapshot_freq == conf.snapshot_freq - 1) or conf.eval_policy(epoch + 1):
        torch.save(net.state_dict(), current_snapshot_path)
    torch.save({'state_dict':net.state_dict(), 'epoch':epoch}, pathjoin(conf.model_path, 'latest.pthl'))
    
    # Printing Train Log
    end_time = datetime.datetime.now()
    print("Epoch {} Training Finished\t Training Loss {}\t Training Acc {}\t lr:{:.4g} \t Runtime: {}\t Logtime: {}".format(epoch + 1, avg_loss, avg_acc, scheduler.get_last_lr()[0], end_time - start_time, end_time))
    
    # Validation Process
    if validation_arg:
        start_time = datetime.datetime.now()
        val_meta = test_model(net, val_loader, conf.criterion, device, progress_bar=False)
        val_loss = val_meta['loss']
        val_acc = val_meta['acc']
        writer.add_scalar('val_loss', val_loss, step_count)
        writer.add_scalar('val_acc', val_acc, step_count)
        end_time = datetime.datetime.now()
        print("Epoch {} Validation Finished\t Validation Loss {}\t Validation acc {}\t Runtime: {}\t Logtime: {}".format(epoch + 1, val_loss, val_acc, end_time - start_time, end_time))
    
    # Early stopping by criterion
    if avg_loss < conf.stop_by_criterion:
        print("Criterion less than designated value, abort training.")
        break
    scheduler.step()

print('Finished Training. The last snapshot saved to final.pth')

final_snapshot_path = pathjoin(conf.model_path, 'final.pth')
torch.save(net.state_dict(), final_snapshot_path)

print("Evaluating models with top {} eigenvectors".format(conf.eigenthings_topn))
eval_last_command = """python3 sbatch_run.py  --exclude="gpu-compute[1-3],linux56" -x="train-m" -r="python3 eval_epoch.py -n={} -m='{}'" --specialinfo="final_eval" """.format(conf.eigenthings_topn, final_snapshot_path)
auto_eval_command = "python3 auto_eval.py -run='{}' -n={} -t={}".format(args.run, conf.eigenthings_topn, conf.auto_eval_thread_num)
print(eval_last_command)
os.system(eval_last_command)

if conf.auto_eval_thread_num > 0:
    print(auto_eval_command)
    os.system(auto_eval_command)
