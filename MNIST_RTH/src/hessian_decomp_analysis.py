import os, sys
import torch
import numpy as np
sys.path.append(os.getcwd())
from config import Config # pylint: disable=no-name-in-module
from algos.closeformhessian import hessianmodule
from IPython import embed
sd = "./experiment_log/run_2/models/final.pth"
real_comp = "./experiment_log/run_2/models/final.pth_LW_ET1000.eval"
exp_name = os.getcwd().split('/')[-1]

conf = Config()
net = conf.net()
net.load_state_dict(torch.load(sd, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, ['fc1', 'fc2', 'fc3'], RAM_cap=64)
comp_layers = ['fc1', 'fc2', 'fc3']
crop_ratio = 0.01
# comp_layers = ['fc3']

eigenthings_real = torch.load(real_comp, map_location='cpu')
eigenvals_real = eigenthings_real['eigenvals_layer']
eigenvecs_real = eigenthings_real['eigenvecs_layer']

def overlap_analysis_layer(n, comp_layers):
    overlap_xs, overlap_ys = [], []
    name = 'Estimate_eigenspace_overlap_{}_{}'.format(exp_name, n)

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=n, dataset_crop=crop_ratio)
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        overlap_dim_x = HM.utils.increasing_interval_dist(1, n, r=1.1)
        print(H_eigenvecs_est.size(), H_eigenvecs.size())
        overlap_dim_y = HM.measure.trace_overlap_trend(H_eigenvecs, H_eigenvecs_est, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)

def sample_eigenvectors(topn, comp_layers):
    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    mats = []
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        mats_base, descs_base = [], []
        mats_est, descs_est = [], []
        name = "Sample_{}_{}".format(exp_name, layer)
        for i in range(topn):
            mats_base.append(HM.utils.reshape_to_layer(H_eigenvecs[i], layer))
            mats_est.append(HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer))
            descs_base.append("Base:{}".format(i))
            descs_est.append("Est:{}".format(i))
        HM.vis.plot_matsvd([mats_base, mats_est], [descs_base, descs_est], name, colormap='Reds')

def singular_vec_overlap_analysis(topn, comp_layers):
    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    E_xs = HM.E_x(comp_layers, dataset_crop=crop_ratio)
    E_xxTs = HM.E_xxT(comp_layers, dataset_crop=crop_ratio)
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        name = "SVOA_{}_{}_{}".format(exp_name, layer, topn)
        l_ovls, r_ovls = [], []
        for i in range(topn):
            vec_base_mat = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            vec_est_mat = HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer)
            U_base, S_base, V_base = torch.svd(vec_base_mat, compute_uv=True) # pylint: disable=no-member
            U_est, S_est, V_est = torch.svd(vec_est_mat, compute_uv=True) # pylint: disable=no-member
            for mat in [U_base, V_base, U_est, V_est]:
                mat.transpose_(0, 1)
            l_ovl = U_base[0].dot(U_est[0]) ** 2
            r_ovl = V_base[0].dot(V_est[0]) ** 2
            l_ovls.append(l_ovl.cpu().numpy())
            r_ovls.append(r_ovl.cpu().numpy())
        overlap_x = [list(range(1, topn + 1)) for i in range(2)]
        overlap_y = [l_ovls, r_ovls]
        descs = ['L Singular Vector Dot^2', 'R Singular Vector Dot^2']
        HM.vis.plots(overlap_x, overlap_y, descs, x_label='* of eigenvec', name=name, line_style='', marker_style='.')

def mats_decomp_visualization(comp_layers):
    E_xxTs = HM.E_xxT(comp_layers, dataset_crop=crop_ratio)
    E_UTAUs = HM.E_UTAU(comp_layers, dataset_crop=crop_ratio)
    mats_UTAU, mats_xxT = [], []
    descs_UTAU, descs_xxT = [], []
    name = "Decomp_{}".format(exp_name)

    for layer in comp_layers:
        mats_UTAU += [E_UTAUs[layer]]
        descs_UTAU += ['E[U^TAU]_{}'.format(layer)]
        mats_xxT += [E_xxTs[layer]]
        descs_xxT += ['E[xx^T]_{}'.format(layer)]
    
    mats = [mats_UTAU, mats_xxT]
    descs = [descs_UTAU, descs_xxT]
    HM.vis.plot_matsvd(mats, descs, name, colormap='viridis')

def kron_pairs_visualization(topn, comp_layers):
    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    E_xxTs = HM.E_xxT(comp_layers, dataset_crop=crop_ratio)
    E_UTAUs = HM.E_UTAU(comp_layers, dataset_crop=crop_ratio)
    
    for layer in comp_layers:
        name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAUs[layer], symmetric=True) # pylint: disable=no-member
        eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True) # pylint: disable=no-member

        est_l = []
        est_r = []
        base_l = []
        base_r = []

        for i in range(topn):
            vec_base_mat = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            vec_est_mat = HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer)
            U_base, S_base, V_base = torch.svd(vec_base_mat, compute_uv=True) # pylint: disable=no-member
            U_est, S_est, V_est = torch.svd(vec_est_mat, compute_uv=True) # pylint: disable=no-member
            est_l.append(U_est[:, :1])
            base_l.append(U_base[:, :1])
            est_r.append(V_est[:, :1])
            base_r.append(V_base[:, :1])

        est_L_dot = HM.utils.mm(eigenvecs_UTAU, torch.cat(est_l, dim=1)) ** 2 # pylint: disable=no-member
        base_L_dot = HM.utils.mm(eigenvecs_UTAU, torch.cat(base_l, dim=1)) ** 2 # pylint: disable=no-member
        est_R_dot = HM.utils.mm(eigenvecs_xxT, torch.cat(est_r, dim=1)) ** 2 # pylint: disable=no-member
        base_R_dot = HM.utils.mm(eigenvecs_xxT, torch.cat(base_r, dim=1)) ** 2 # pylint: disable=no-member

        mats = [[est_L_dot[:topn], est_R_dot[:topn]], [base_L_dot[:topn], base_R_dot[:topn]]]
        descs = [["est_L", "est_R"], ["base_L", "base_R"]]
        HM.vis.plot_mat(mats, descs, name, w=12, v_min=-1, v_max=1, colormap='bwr')
    
#sample_eigenvectors(5, comp_layers)
#overlap_analysis_layer(200, comp_layers)
#singular_vec_overlap_analysis(200, comp_layers)
#mats_decomp_visualization(comp_layers)
kron_pairs_visualization(100, comp_layers)

