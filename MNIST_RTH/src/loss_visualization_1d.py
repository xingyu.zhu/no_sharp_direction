import os, sys, glob
sys.path.append(os.getcwd())

import torch
import torch.backends.cudnn as cudnn
from torch.utils.data import DataLoader
import numpy as np
import pynvml

import torch
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
from scipy.interpolate import CubicSpline
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import plotly.express as px
import plotly.graph_objects as go
import plotly
import pandas as pd
import itertools

import hashlib, json, argparse, os, datetime
from copy import deepcopy
from tqdm import tqdm
from termcolor import colored
from os.path import join as pathjoin
from collections import OrderedDict

import utils
from config import Config # pylint: disable=no-name-in-module
from test import test_model

conf = Config()
# conf = Config()

def loss_landscape_nd(net, dataloader, criterion, configs, device):
    """
    Config: a n x 4 list
    each row indicates start, end, number of points, random seed (None for random)
    """
        
    sd_base = deepcopy(net.state_dict())

    sd_v_ls, grid_ls = [], []
    for i in range(len(configs)):
        print(configs[i][:-1])
        start, end, num, vec = configs[i]
        grid_ls.append(np.linspace(start, end, num))
        sd_v_ls.append(vec)
    
    grid = utils.cartesian_product_itertools(grid_ls)
    loss_landscape = np.zeros(len(grid))

    for i, s in enumerate(grid):
        begin_time = datetime.datetime.now()
        sd = sd_base
        for j in range(len(configs)):
            sd = utils.state_dict_add(sd, utils.state_dict_scalar_mul(sd_v_ls[j], s[j]))
        net.load_state_dict(sd)
        sd_loss = test_model(net, dataloader, criterion, device, progress_bar=False)
        loss_landscape[i] = sd_loss['loss']
        end_time = datetime.datetime.now()

        print("# {}/{}\t loss: {}\t took {}".format(i + 1, len(grid), sd_loss['loss'], end_time - begin_time))
    net.load_state_dict(sd_base)
    return loss_landscape, grid, grid_ls

def create_logname(model_path, length, density, using_testset, tag):

    model_abbrev = '-'.join(model_path.split('/')[-4:])
    if using_testset:
        dataset_desc = conf.testset_info
    else:
        dataset_desc = conf.trainset_info
    name = '{}_{}__{}__R{}__D{}.landscape'.format(tag, model_abbrev, dataset_desc, length, density)
    return name

def compute_1d_landscape(net, model_path, length, density, using_testset, d_vecs=None, vec_num=None, filterwise_rescale=None, track_random=True):

    net_base, device, _ = utils.prepare_net(net, conf.use_gpu)

    start_time = datetime.datetime.now()
    if using_testset:
        print("using test set")
        test_set = conf.dataset(train=False, transform=conf.test_transform)
        dataloader = DataLoader(test_set, batch_size=conf.testing_batchsize, shuffle=False, num_workers=conf.test_provider_count)
    else:
        print("using train set")
        train_set = conf.dataset(train=True, transform=conf.test_transform)
        dataloader = DataLoader(train_set, batch_size=conf.training_batchsize, shuffle=False, num_workers=conf.test_provider_count)

    param_model = torch.load(model_path)
    net_base.load_state_dict(param_model)

    sd_base = deepcopy(net_base.state_dict())
    
    if d_vecs is None:
        d_vecs = []
        print('randomly generating directional vectors')
        for i in range(vec_num):
            if track_random:
                seed = i
            else:
                seed = np.random.randint(100000)
            if filterwise_rescale:
                d_vecs.append(utils.gaussian_random_direction_filterwise_rescaled(sd_base, random_seed=seed))
            else:
                d_vecs.append(utils.gaussian_random_direction(sd_base, random_seed=seed))
    assert len(d_vecs) >= vec_num
    d_vecs = d_vecs[:vec_num]
    
    loss_landscapes, grids, grid_lss = [], [], []

    for i, d_vec in enumerate(d_vecs):
        print('Calculating {}/{}'.format(i + 1, vec_num))
        config = [[-length, length, density, d_vec]]
        loss_landscape, grid, grid_ls = loss_landscape_nd(net_base, dataloader, conf.criterion, config, device)
        loss_landscapes.append(loss_landscape)
        grids.append(grid)
        grid_lss.append(grid_ls)
        
    print("Computing done, Runtime: {}".format(datetime.datetime.now() - start_time))

    return loss_landscapes, grids, grid_lss

def load_eigenthings(model_path, target_device):

    tmp = glob.glob(model_path + "*.eigenthings")
    print('loading eigenthings from {}'.format(tmp[0]))
    assert len(tmp) > 0, 'eigenthings need to be computed before visualization'
    eigenvals, eigenvecs = torch.load(tmp[0], map_location=target_device)
    return eigenvals, eigenvecs

def load_from_landscape(load_landscape_path):
    
    if '/' not in load_landscape_path:
        load_landscape_path = os.path.join(conf.vis_dir, load_landscape_path)
    assert os.path.isfile(load_landscape_path), 'Invalid landscape path by the -L argument'
    loss_landscape, grid, grid_ls, vals = torch.load(load_landscape_path)
    return loss_landscape, grid, grid_ls, vals

    # loss_landscape, grid = torch.load(load_landscape_path)
    # return loss_landscape, grid


def visualize_landscape_1d_mpl(landscapes_1d, grids_1d, grid_lss, vals, info='loss landscape', save_dirc=None, show=False):

    print('visualize using Matplotlib')
    cmap = cm.plasma # pylint: disable=no-member
    
    if not show:
        mpl.use('Agg')
    
    fig = plt.figure()
    full_landscape = np.array(landscapes_1d).flatten()
    max_cut = np.sort(full_landscape)
    cut = max_cut[int(0.95 * len(max_cut))]

    if vals is None:
        labels = ['v{}'.format(i + 1) for i in range(len(landscapes_1d))]
    else:
        labels = ['λ{}={:.2g}'.format(i + 1, vals[i]) for i in range(len(landscapes_1d))]

    ax=fig.gca(projection='3d')
    for i in range(0, len(landscapes_1d)):
        landscape = np.array(landscapes_1d[i])
        domain = grid_lss[i][0]
        cubic_interp = CubicSpline(domain, landscape)
        interp_x = np.linspace(min(domain), max(domain), 100)
        interp_y = cubic_interp(interp_x)

        y_base = np.ones_like(interp_x) * (i + 1)
        ax.plot(interp_x, y_base, interp_y, color=cmap(i / len(landscapes_1d)))

    ax.set_ylim3d(len(landscapes_1d) + 1, 0)
    ax.set_yticks(np.arange(0, len(landscapes_1d) + 1))
    ax.set_zlim3d(0, cut)
    ax.set_yticklabels(labels, rotation=-45)
    for tick in ax.get_xticklabels():
        tick.set_rotation(45)
        tick.set_rotation_mode('anchor')
    ax.set_zlabel('Train Loss')
    if save_dirc is not None:
        print('saving to {}'.format(save_dirc))
        plt.savefig(save_dirc, dpi=220, bbox_inches = 'tight')
    if show:
        plt.show()

def visualize_landscape_1d_plotly(landscapes_1d, grids_1d, grid_lss, vals, info='loss landscape', save_html=None, show=False):

    if vals is None:
        labels = ['v{}'.format(i + 1) for i in range(len(landscapes_1d))]
    else:
        labels = ['λ{}={:.2g}'.format(i + 1, vals[i]) for i in range(len(landscapes_1d))]

    full_landscape = np.array(landscapes_1d).flatten()
    max_cut = np.sort(full_landscape)
    cut = max_cut[int(0.95 * len(max_cut))]

    X, Y, Z = np.zeros((len(landscapes_1d), 100)), np.zeros((len(landscapes_1d), 100)), np.zeros((len(landscapes_1d), 100))
    label_ls = []
    for i in range(0, len(landscapes_1d)):
        landscape = np.array(landscapes_1d[i])
        domain = grid_lss[i][0]
        cubic_interp = CubicSpline(domain, landscape)
        interp_x = np.linspace(min(domain), max(domain), 100)
        interp_y = cubic_interp(interp_x)
        y_base = np.ones_like(interp_x) * (i + 1)
        X[i] = interp_x
        Y[i] = y_base
        Z[i] = interp_y
        label_ls += [labels[i] for j in range(len(interp_x))]
    X, Y, Z = X.flatten(), Y.flatten(), Z.flatten()

    df = pd.DataFrame({
        "Shift": X, 
        "Vec": Y, 
        "Train Loss": Z,
        "Info":label_ls
    })
    df["Vec"] = df["Vec"].astype(float)
    fig = px.line_3d(df, x='Shift', y='Vec', z='Train Loss', color='Vec',color_discrete_sequence= px.colors.sequential.Plasma)
    fig.update_layout(scene_aspectmode='manual', scene_aspectratio=dict(x=1, y=1, z=(max_cut[-1]/cut)))
    camera = dict(center=dict(x=0, y=0, z= - max_cut[-1]/(2 * cut) + 0.25))
    fig.update_layout(scene_camera=camera)
    fig.update_layout(scene = dict(
                    yaxis = dict(
                        ticktext= labels,
                        tickvals= np.arange(len(landscapes_1d)) + 1),
                        yaxis_title=''
                  ))
    fig.update_layout(
        title=info + """\n<a href="https://www2.cs.duke.edu/~xz231">Back to homepage</a>""",
    )
    if save_html is not None:
        print('exporting html')
        plotly.offline.plot(fig, filename=save_html, auto_open=False)
    if show:
        fig.show()

def main_1d():

    parser = argparse.ArgumentParser()
    parser.add_argument('-run', type=str, help='The indicator of run', default='1')
    g = parser.add_mutually_exclusive_group(required=True)

    g.add_argument('-m', type=str, help='The param model as the center of the plot')
    g.add_argument('-l', type=str, help='Load specific output')
    g.add_argument('-x', action='store_true', help='Visualize new existing .landscape files')
    g.add_argument('-xx', action='store_true', help='Visualize all existing .landscape files')

    parser.add_argument('-o', action='store_true', help='Overwrite existing result or not')
    parser.add_argument('-i', action='store_true', help='Output image of landscape?')
    parser.add_argument('-w', action='store_true', help='Output interactive webpage (plotly) of landscape?')
    
    parser.add_argument('-r', type=float, default=0.1, help='range of landscape')
    parser.add_argument('-d', type=int, default=15, help='mesh density')
    parser.add_argument('-t', action='store_true', help='Using Test Set')

    h = parser.add_mutually_exclusive_group(required=True)

    h.add_argument('-R1DN', action='store_true', help='1d random direction filterwise normalized')
    h.add_argument('-R1D', action='store_true', help='1d random direction')
    h.add_argument('-E1D', action='store_true', help='1d dominant eigenspace')
    
    parser.add_argument('-vecnum', type=int, help='The number of vectors to compute along', required=True)
    args = parser.parse_args()
    
    global conf
    conf = Config(run_number=args.run)

    if not os.path.isdir(conf.vis_dir):
        os.makedirs(conf.vis_dir)
    
    length, density = args.r, args.d
    using_testset = args.t
    load_landscape_path = args.l
    model_path = args.m
    out_img, out_html = args.i, args.w
    force_overwrite = args.o

    if args.R1DN:
        tag = "1D_randvecftnorm"
    if args.R1D:
        tag = "1D_randvec"
    if args.E1D:
        tag = "1D_domeigenspace"

    print(args)

    if args.x or args.xx:
        fl_ls = []
        for x in os.listdir(conf.vis_dir):
            if x.endswith('.landscape') and ("1D_" in x):
                complete_path = pathjoin(conf.vis_dir, x)
                if args.xx:
                    if not os.path.isfile(complete_path + '.jpg'):
                        fl_ls.append(complete_path)
                else:
                    fl_ls.append(complete_path)
        print(fl_ls)

        for i, x_path in enumerate(fl_ls):
            print("{}/{} Dealing with {}".format(i + 1, len(fl_ls), x_path))
            loss_landscape, grid, grid_ls, vals = torch.load(x_path)
            visualize_landscape_1d_mpl(loss_landscape, grid, grid_ls, vals, save_dirc=x_path + '.jpg')
            visualize_landscape_1d_plotly(loss_landscape, grid, grid_ls, vals, save_html=x_path + '.html')
        return

    comp = True

    # Read directedly from a .landscape file
    if load_landscape_path is not None:
        comp = False
        loss_landscape, grid, grid_ls, vals = load_from_landscape(load_landscape_path)
        log_path = load_landscape_path

    else:
        assert model_path is not None, 'You must specify one among -L and -M'
        if '/' not in model_path:
            model_path = os.path.join(conf.model_path, model_path)
        assert os.path.isfile(model_path), 'Invalid model path by the -M argument'
        comp = True

        log_name = create_logname(model_path, length, density, using_testset, tag)
        log_path = os.path.join(conf.vis_dir, log_name)

        if os.path.isfile(log_path):
            print('This model has been previously calculated.')
            if not force_overwrite:
                print('Loading from previous records. Overwritting can be manually configured.')
                loss_landscape, grid, grid_ls, vals = load_from_landscape(log_path)
                comp = False
    
    info = 'Loss Landscape'
    if args.R1DN:
        info = "Loss Landscape Random Filterwise Normalized"
    if args.R1D:
        info = "Loss Landscape Random"
    if comp:
        vals = None
        if args.R1DN:
            net = conf.net()
            loss_landscape, grid, grid_ls = compute_1d_landscape(net, model_path, length, density, using_testset, d_vecs=None, vec_num=args.vecnum, filterwise_rescale=True, track_random=True)
        if args.R1D:
            net = conf.net()
            loss_landscape, grid, grid_ls = compute_1d_landscape(net, model_path, length, density, using_testset, d_vecs=None, vec_num=args.vecnum, filterwise_rescale=False, track_random=True)
        if args.E1D:
            net = conf.net()
            _, device, _ = utils.prepare_net(net, conf.use_gpu)
            net = conf.net()
            eigenvals, eigenvecs = load_eigenthings(model_path, target_device=device)
            assert len(eigenvals) >= 2
            if not eigenvals[0] > eigenvals[1]:
                eigenvecs.reverse()
                if isinstance(eigenvals, list):
                    eigenvals.reverse()
                else:
                    eigenvals = eigenvals[::-1]
                print(eigenvals)
                print("reverse sequence of eigenvectors")
            # v1.to(device)
            vals = eigenvals
            loss_landscape, grid, grid_ls = compute_1d_landscape(net, model_path, length, density, using_testset, d_vecs=eigenvecs, vec_num=args.vecnum, filterwise_rescale=False, track_random=True)
            info = 'Dominating Eigenvalues'
        torch.save([loss_landscape, grid, grid_ls, vals], log_path)
        # torch.save([loss_landscape, grid, grid_ls], 'sample1d.landscape')
        print('Landscape saved to {}'.format(log_path))

    if out_img:
        image_path = log_path + '.jpg'
        visualize_landscape_1d_mpl(loss_landscape, grid, grid_ls, vals, save_dirc=image_path, info=info)
    
    if out_html:
        html_path = log_path + '.html'
        visualize_landscape_1d_plotly(loss_landscape, grid, grid_ls, vals, save_html=html_path, info=info)

if __name__ == "__main__":
    main_1d()
